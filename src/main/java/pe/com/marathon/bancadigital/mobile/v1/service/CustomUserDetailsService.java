package pe.com.marathon.bancadigital.mobile.v1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pe.com.marathon.bancadigital.mobile.v1.entity.User;
import pe.com.marathon.bancadigital.mobile.v1.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String user) throws UsernameNotFoundException {
        Optional<User> usuario = userRepository.findByUsuario(user);
        if(usuario.isPresent()){
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            grantedAuthorities.add(new SimpleGrantedAuthority("ADMIN"));
            return new org.springframework.security.core.userdetails.User(usuario.get().getUsuario(),
                    usuario.get().getPassword(), grantedAuthorities);
        }else{
            throw new UsernameNotFoundException("No se encuentra usuario");
        }
    }
}
