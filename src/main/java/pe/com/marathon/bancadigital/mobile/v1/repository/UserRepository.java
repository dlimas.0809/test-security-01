package pe.com.marathon.bancadigital.mobile.v1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.com.marathon.bancadigital.mobile.v1.entity.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findByUsuario(String usuario);

}
