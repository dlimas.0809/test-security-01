package pe.com.marathon.bancadigital.mobile.v1.config.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException error) throws IOException, ServletException {
        log.debug("Jwt authentication failed:" + error);
        final Map<String, Object> mapBodyException = new HashMap<>();
        log.error(error.getLocalizedMessage(), error);
        mapBodyException.put("error", error.getMessage());
        mapBodyException.put("message", error.getMessage());
        mapBodyException.put("path", request.getServletPath());
        mapBodyException.put("timestamp", (new Date()).getTime());

        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        final ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(response.getOutputStream(), mapBodyException);
        log.error("Fin de generar custom error");
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED	, "Jwt authentication failed");

    }

}