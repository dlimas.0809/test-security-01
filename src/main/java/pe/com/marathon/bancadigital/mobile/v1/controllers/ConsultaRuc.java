package pe.com.marathon.bancadigital.mobile.v1.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pe.com.marathon.bancadigital.mobile.v1.config.properties.Url;
import pe.com.marathon.bancadigital.mobile.v1.utils.CommonUtil;

@Slf4j
@RestController
public class ConsultaRuc {

    @Autowired
    private Url url;

    @Autowired
    private CommonUtil commonUtil;

    @GetMapping(value = "/consultaRuc", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> consultaRuc(@RequestParam(name = "ruc") String ruc)
            throws Exception {
        String url1 = String.format(url.getVerificacion(), ruc);
        String pinBlock = CommonUtil.getApiUrl(url1, ruc, String.class);
        return ResponseEntity.ok(pinBlock);
    }


}
