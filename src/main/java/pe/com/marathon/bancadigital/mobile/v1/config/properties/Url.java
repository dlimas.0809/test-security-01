package pe.com.marathon.bancadigital.mobile.v1.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "url")
@Setter
@Getter
public class Url {

    private String verificacion;

    private String queryuser;
}
