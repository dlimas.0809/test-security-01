package pe.com.marathon.bancadigital.mobile.v1.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class CommonUtil {

    public static <T> T getApiUrl(String url, T request, Class<T> tclass) throws Exception {
        try {
            ResponseEntity<T> response;
            RestTemplate restTemplate = null;
            HttpHeaders headers = null;
            List<MediaType> mediaTypes = null;
            HttpEntity<T> httpEntity = null;
            headers = new HttpHeaders();
            mediaTypes = new ArrayList<>();
            mediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
            headers.setAccept(mediaTypes);
            headers.setContentType(MediaType.APPLICATION_JSON);
            httpEntity = (HttpEntity<T>) new HttpEntity<>(request, headers);
            restTemplate = new RestTemplate();
            log.debug("Params: \n\t- Invocando URL : " + url);
            response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, tclass);
            log.debug("\n\tResponse: " + response);
            return response.getBody();
        } catch (RestClientException e) {
            log.error(e.getMessage());
            throw new Exception("En este momento el servicio no se encuentra disponible");
        }
    }

}
